# 10x-hashing

This readme describes how to process 10X cell hashing (cite-seq) libraries generated by the University of Minnesota Genomics Center.

10X library types:

- GEX: gene expression profiling library (usually 3', but 5' libraries are required for use with TCR libraries)
- HTO: Cell hashing library (cite-seq)
- ADT: Antibody library
- TCR: T and B cell immunity profiling library (VDJ)

A 10X single-cell project using cell hashing will have GEX and HTO libraries, and may also have ADT and TCR libraries.

## Locate data

The UMGC uses Cellranger to demultiplex Illumina sequencing runs to generate a set of fastq.gz files for each library type (GEX, HTO, ADT, and/or TCR). Each set has three fastq files: R1 (forward read), R2 (reverse read), and I1 (index read). The fastq.gz files for each library type are delivered to the customer's data_release folder, with each library in a separate folder and the folder name indicating the libarary type.

Files in /home/beckman/data_release/umgc/novaseq for an example 10X project with GEX, HTO, and ADT libraries. Note that libraries for a project may be on multiple runs:

```
novaseq/
└── 190925_A00223_0218_AHJJ2KDSXX
    ├── Beckman_Project_010-GEX
    │   ├── Beckman_010_A_S1_L003_I1_001.fastq.gz
    │   ├── Beckman_010_A_S1_L003_R1_001.fastq.gz
    │   ├── Beckman_010_A_S1_L003_R2_001.fastq.gz
    ├── Beckman_Project_010-ADT
    │   ├── Beckman_010_ADT_F_S1_L003_I1_001.fastq.gz
    │   ├── Beckman_010_ADT_F_S1_L003_R1_001.fastq.gz
    │   ├── Beckman_010_ADT_F_S1_L003_R2_001.fastq.gz
    └── Beckman_Project_010-HTO
        ├── Beckman_010_HTO_S_S3_L003_I1_001.fastq.gz
        ├── Beckman_010_HTO_S_S3_L003_R1_001.fastq.gz
        └── Beckman_010_HTO_S_S3_L003_R2_001.fastq.gz
```

## Create a libraries file
The libraries file declares the fastq files and library type for each input dataset. The libraries file for the example dataset:

```
fastqs,sample,library_type
/home/beckman/data_release/umgc/novaseq/190925_A00223_0218_AHJJ2KDSXX/Beckman_Project_010-ADT,Beckman_010_ADT_F,Antibody Capture
/home/beckman/data_release/umgc/novaseq/190925_A00223_0218_AHJJ2KDSXX/Beckman_Project_010-GEX,Beckman_010_A,Gene Expression
/home/beckman/data_release/umgc/novaseq/190925_A00223_0218_AHJJ2KDSXX/Beckman_Project_010-HTO,Beckman_010_HTO_S,Custom
```

The libraries file can be automatically generated using 10xlibraries.pl. This script looks in your data_release folder for folders associated with the specified project, and creates a Cellranger libraries file based on the fastq files found:
```
module load umgc
module load gtools
10xlibraries.pl --projectname Beckman_Project_010 --output beckman_010-libraries.csv
```

## Create a feature file

The feature file declares the set of Feature Barcoding (HTO and ADT) reagents used in the experiment. For each unique Feature Barcode used, this file declares a feature name and identifier, the unique Feature Barcode sequence associated with this reagent, and a pattern indicating how to extract the Feature Barcode sequence from the read sequence. See [Feature Barcode Reference](https://support.10xgenomics.com/single-cell-gene-expression/software/pipelines/latest/using/feature-bc-analysis#feature-ref) for details on how to construct the feature reference.

The  feature file for the example dataset:

```
id,name,read,pattern,sequence,feature_type
Sample_control,S1-HT-1,R2,^(BC),ACCCACCAGTAAGAC,Custom
Sample_exp1a,S2-HT-2,R2,^(BC),GGTCGAGAGCATTCA,Custom
Sample_exp2a,S3-HT-7,R2,^(BC),GAGTCTGCCAGTATC,Custom
Sample_exp3a,S4-HT-6,R2,^(BC),TATGCTGCCACGGTA,Custom
Sample_exp1b,S5-HT-9,R2,^(BC),TGCCTATGAAACAAG,Custom
Sample_exp2b,S6-HT-11,R2,^(BC),GCTTACCGAATTAAC,Custom
Sample_exp3b,S7-HT-12,R2,^(BC),CTGCAAATATAACGG,Custom
Sample_exp1c,S8-HT-13,R2,^(BC),CTACATTGCGATTTG,Custom
Sample_exp2c,S9-HT-14,R2,^(BC),CTTTCGCCAACTCTG,Custom
TotalSeq-14,CD11b,R2,^(BC),TGAAGGCTCATTTGT,Antibody Capture
TotalSeq-106,CD11c,R2,^(BC),GTTATGGACGCTTGC,Antibody Capture
TotalSeq-93,CD19,R2,^(BC),ATCAGCCATGTCAGT,Antibody Capture
TotalSeq-426,CD192-(CCR2),R2,^(BC),AGTGCGATCTGCAAC,Antibody Capture
TotalSeq-182,CD3,R2,^(BC),GTATGTCCGCTCGAT,Antibody Capture
TotalSeq-551,CD301a,R2,^(BC),TGTATTTACTCACCG,Antibody Capture
TotalSeq-1,CD4,R2,^(BC),AACAAGACCCTTGAG,Antibody Capture
```

Full documentation on the libraries and feature files is on the 10X website: [Feature barcode analysis](https://support.10xgenomics.com/single-cell-gene-expression/software/pipelines/latest/using/feature-bc-analysis)

## Run Cellranger count

Running cellranger with at least 240GB of memory is recommended. Here is the SLURM file for analyzing the example dataset on the amdsmall partition. The example dataset is a mouse project with 10,000 cells expected, using the 3' chemistry: 

```
#!/bin/bash -l
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=128
#SBATCH --time=24:00:00
#SBATCH --mem=240gb
#SBATCH --mail-type=ALL

module load umgc
module load cellranger
cellranger count --id Beckman_Project_010 --feature-ref beckman_010-features.csv --libraries beckman_010-libraries.csv --transcriptome /home/umgc/public/bin/cellranger/refdata/refdata-cellranger-mm10-3.0.0/ --expect-cells 10000 --localcores=128 --localmem=230 --chemistry SC3Pv3
```

## Troubleshooting HTO barcodes

To see what barcode sequences are present in a HTO or ADT dataset using 5' chemistry run the fastq-starts.pl script on the R2 fastq file:

```
module load umgc
module load gtools
fastq-starts.pl Beckman_010_HTO_S_S3_L003_R2_001.fastq.gz
GGGGGGGGGGGGGGG 1207 1.21%
CTACATTGCGATTTG 4464 4.46%
TGCCTATGAAAcAAg 6553 6.55%
CCCTCTCTGGATTCt 6664 6.66%
GCTTACCGAAttAAc 7178 7.18%
ACCCACCAGTAAGAC 7642 7.64%
CTTTCGCCAACTCTg 7789 7.79%
GAGTCTGCCAGTATC 8326 8.33%
TATGCTGCCACGGtA 10179 10.18%
GGTCGAGAGCATTCA 34015 34.02%
Total reads: 100000
Other reads: 5983 5.98%
```

The output shows the most common sequences present at the beginning of the reads (for the first 100,000 reads). In this case they match the expected barcodes listed in the feature file. The poly-G sequence indicates a failed R2 sequencing reaction (no data for the read).

For TotalSeq-B or -C HTO libraries the barcode sequence starts 10 bases into the read. Add the "-i 10" option when running fastq-starts to ignore the first 10 bases of each read.
